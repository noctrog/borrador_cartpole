#pragma once

#include <tuple>

#include <Newton.h>
#include <Entity.h>
#include <Environment.h>

namespace rl
{
    struct NewtonDeleter {
        void operator()(NewtonWorld* world) {
            NewtonDestroyAllBodies(world);
            NewtonDestroy(world);
        }
    };

    class CartPoleEnv final : private Environment
    {
    public:
        using Observation = std::array<float, 4>;
        enum class Action : std::size_t {
            LEFT = 0,
            RIGHT,
            NONE
        };

        CartPoleEnv(size_t max_steps = 0);
        virtual ~CartPoleEnv();

        std::tuple<Observation, float, bool> 
        step(Action a);

        Observation reset(void);

    private:
        void setup_world(void);
        void update_physics(void);
        Observation get_state(void);

        std::unique_ptr<NewtonWorld, NewtonDeleter> world_;
        std::unique_ptr<BoxEntity>  cart_;
        std::unique_ptr<BoxEntity>  pole_;
        std::unique_ptr<dCustomHinge> hinge_;
        std::unique_ptr<dCustomSlider> slider_;

        Action current_action_;

        const size_t max_steps_;
        size_t steps_;
    };
}
