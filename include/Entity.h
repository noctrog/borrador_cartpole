#pragma once

#include <memory>

#include <glm/glm.hpp>
#include <dNewton.h>
#include <BoxPrimitive.h>

struct NewtonDeleter
{
    void operator()(NewtonBody* body) { NewtonDestroyBody(body); };
};

class BoxEntity {
    public:
        BoxEntity(const NewtonWorld* world,
                  dMatrix const& tm,
                  float mass,
                  glm::vec3 const& size,
                  glm::vec3 const& color);
        ~BoxEntity();

        dMatrix& get_tm(void);
        void set_tm(dMatrix const& tm);

        NewtonBody* const get_dyn(void);

        void draw(glm::mat4 const& pers_, glm::mat4 const& view);
        void set_user_data(void* data);
    private:
        dMatrix tm_;
        std::unique_ptr<NewtonBody, NewtonDeleter> dyn_;

        rndr::BoxPrimitive primitive_;
        rndr::Shader       shader_;
};
