#pragma once

#include <tuple>
#include <memory>

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

namespace glfw
{
    struct GLFW_Deleter
    {
        void operator()(GLFWwindow* ptr) { if (ptr) glfwDestroyWindow(ptr); }
    };
}

namespace rl
{
    class Environment
    {
    public:
        Environment();
        virtual ~Environment () = 0;

        //template<typename O>
        //virtual std::tuple<O, float, bool> reset(void);
        
        void fb_size_callback(const int width, const int height);
    protected:
        // Internal state
        bool bGraphics_;

        // Graphics
        void init_graphics(void);
        void stop_graphics(void);

        static void fb_size_callback(GLFWwindow* window, const int width, const int height);
        static void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos);
        static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
        void update_perspective_matrix(void);
        glm::mat4 perspective_;
        struct {
            int width;
            int height;
        } w_;
        std::unique_ptr<GLFWwindow, ::glfw::GLFW_Deleter> window_;

    private:
        void setup(void);
        void close(void);

        // Graphics
        void glfw_setup();
        void opengl_setup();
        void glfw_close();
        void opengl_close();
    };
}
