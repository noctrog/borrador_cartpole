#include <iostream>
#include <random>

#include <CartPoleEnv.h>
#include <dMatrix.h>

namespace rl
{
    CartPoleEnv::CartPoleEnv(size_t max_steps)
        : max_steps_(max_steps), steps_(0), current_action_(CartPoleEnv::Action::NONE)
    {
        setup_world();
    }

    CartPoleEnv::~CartPoleEnv()
    {

    }

    CartPoleEnv::Observation CartPoleEnv::get_state(void)
    {
        // Calculate x coordinate of pole
        float x_pos = pole_->get_tm().m_posit.m_x;
        // Calculate speed in the x coordinate og the pole
        auto temp = dVector {};
        NewtonBodyGetVelocity(pole_->get_dyn(), &temp[0]);
        float x_vel = temp[0];
        // Calculate pole angle
        float angle = hinge_->GetJointAngle();
        // Calculate pole angular speed
        float ang_speed = hinge_->GetJointOmega();

        return { x_pos, x_vel, angle * dRadToDegree, ang_speed * dRadToDegree};
    }

    std::tuple<CartPoleEnv::Observation, float, bool>
    CartPoleEnv::step(CartPoleEnv::Action a)
    {
        if (max_steps_ > 0 && steps_ == max_steps_)
            return { get_state(), 0.0f, true };

        current_action_ = a;

        update_physics();

        if (max_steps_ > 0) steps_++;

        auto state = get_state();

        bool is_done = (steps_ == max_steps_) || (pole_->get_tm().m_posit.m_y < 1.44f);

        return { state, 1.0f, is_done };    
    }

    void CartPoleEnv::setup_world(void)
    {
        // Random number generator, used for randomizing the initial state
        std::random_device rd {};
        auto mtgen = std::mt19937{ rd() };
        auto ud = std::uniform_real_distribution<float>(-1.0f, 1.0f);

        // Create new physics world
        world_.reset(NewtonCreate());

        // Create cart, at random location
        const float x_ini = 0.0f * ud(mtgen);
        dMatrix tm_cart { dGetIdentityMatrix() };
        tm_cart[3] = dVector{x_ini, 0.0f, -20.0f};
        cart_.reset(new BoxEntity(world_.get(), tm_cart, 1.0f, 
                                  { 2.0, 1.0, 1.0 }, 
                                  {0.8157f, 0.5294f, 0.4392f}));
        cart_->set_user_data(this);

        // Create slider
        dMatrix pivot { dGetIdentityMatrix() };
        pivot.m_posit.m_z -= 20.0f;
        slider_.reset(new dCustomSlider(tm_cart, pivot, cart_->get_dyn(), nullptr));
        slider_->SetLimits(-4.0f, 4.0f);
        slider_->EnableLimits(true);
        slider_->SetFriction(0.0005f);

        // Create Pole
        float pole_angle = 0.0f;
        while (std::abs(pole_angle) < 0.03f) {
            pole_angle = 0.1f * ud(mtgen);
        }
        dMatrix tm_pole { dRollMatrix(pole_angle) };
        tm_pole[3] = dVector { x_ini - 1.5f * std::sin(pole_angle),
            1.5f * std::cos(pole_angle), -19.5f };
        pole_.reset(new BoxEntity(world_.get(), tm_pole, 0.1f, 
                                  { 0.1, 3.0, 0.1 }, 
                                  { 0.7490f, 0.3804f, 0.4157f }));

        // Create Hinge joint
        pivot = dMatrix(dYawMatrix(dPi * 0.5f));
        pivot.m_posit.m_x = x_ini;
        pivot.m_posit.m_z = -19.35f;
        hinge_.reset(new dCustomHinge(pivot, pole_->get_dyn(), cart_->get_dyn()));

        NewtonBodySetDestructorCallback(cart_->get_dyn(), [](const NewtonBody* body){});
        NewtonBodySetTransformCallback(cart_->get_dyn(), [](const NewtonBody* body, const dFloat* matrix, int thread_id){
                // Get current transform
                CartPoleEnv* env = (CartPoleEnv*)NewtonBodyGetUserData(body);
                // Update transform
                NewtonBodyGetMatrix(body, &env->cart_->get_tm()[0][0]);
            });
        NewtonBodySetForceAndTorqueCallback(cart_->get_dyn(), [](const NewtonBody* body, dFloat timestep, int thread_id){
                dFloat mass, Ixx, Iyy, Izz;
                NewtonBodyGetMass(body, &mass, &Ixx, &Iyy, &Izz);
                dVector gravity {0.0f, -9.81f * mass, 0.0f, 1.0f};
                NewtonBodyAddForce(body, &gravity[0]);

                CartPoleEnv* env = (CartPoleEnv*)NewtonBodyGetUserData(body);

                float force = 20.0f;
                switch (env->current_action_) {
                    case CartPoleEnv::Action::LEFT:
                        force = -force;
                        break;
                    case CartPoleEnv::Action::RIGHT:
                        break;
                    case CartPoleEnv::Action::NONE:
                    default:
                        force = 0.0f;
                        break;
                }

                //float f = 50.0 * std::cos(0.767*glfwGetTime()) * std::sin(glfwGetTime()*1.33 + 0.2);
                dVector f_dir { force, 0.0f, 0.0f, 1.0f };
                NewtonBodyAddForce(body, &f_dir[0]);
            });

        NewtonBodySetDestructorCallback(pole_->get_dyn(), [](const NewtonBody* body){});
        NewtonBodySetTransformCallback(pole_->get_dyn(), [](const NewtonBody* body, const dFloat* matrix, int thread_id){
                // Get current transform
                BoxEntity* ent = (BoxEntity*)NewtonBodyGetUserData(body);
                // Update transform
                NewtonBodyGetMatrix(body, &ent->get_tm()[0][0]);
            });
        NewtonBodySetForceAndTorqueCallback(pole_->get_dyn(), [](const NewtonBody* body, dFloat timestep, int thread_id){
                dFloat mass, Ixx, Iyy, Izz;
                NewtonBodyGetMass(body, &mass, &Ixx, &Iyy, &Izz);
                dVector gravity {0.0f, -9.81f * mass, 0.0f, 1.0f};
                NewtonBodyAddForce(body, &gravity[0]);
            });
    }

    void CartPoleEnv::update_physics(void)
    {
        // Clear OpenGL framebuffer and depth buffer
        glClearColor(0.1804f, 0.2039f, 0.2510f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        glClear(GL_DEPTH_BUFFER_BIT);

        /******************
        *  Physics step  *
        ******************/
        NewtonUpdate(world_.get(), 1.0f / 60.0f);

        /*****************
        *  Render step  *
        *****************/
        
        glm::mat4 view = glm::mat4(1.0f);

        // Cart
        cart_->draw(perspective_, view);

        // Pole
        pole_->draw(perspective_, view);

        glfwSwapBuffers(window_.get());
        glfwPollEvents();
    }

    CartPoleEnv::Observation CartPoleEnv::reset(void)
    {
        steps_ = 0;

        world_.release();
        cart_.release();
        pole_.release();
        hinge_.release();
        slider_.release();

        setup_world();

        return get_state();
    }
}
