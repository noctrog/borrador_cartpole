#include "BoxPrimitive.h"
#include <Entity.h>

BoxEntity::BoxEntity(const NewtonWorld* world,
                     dMatrix const& tm,
                     float mass,
                     glm::vec3 const& size,
                     glm::vec3 const& color)
    : tm_(tm), primitive_(size), shader_("thirdparty/renderer/src/shaders/vertex.glsl",
                        "thirdparty/renderer/src/shaders/primitive_fragment.glsl")
{
    // Physics
    NewtonCollision* cs = NewtonCreateBox(world, size[0], size[1], size[2], 0, nullptr);
    dyn_.reset(NewtonCreateDynamicBody(world, cs, &tm[0][0]));
    NewtonBodySetMassMatrix(dyn_.get(), mass, 1.0f, 1.0f, 1.0f);
    NewtonBodySetUserData(dyn_.get(), this);
    NewtonDestroyCollision(cs);
    cs = nullptr;

    // Graphics
	shader_.use();
	shader_.setInt("material.diffuse", 0);
	shader_.setVec3("viewPos", {0.0, 0.0, 0.0});
    shader_.setVec3("material.diffuse", color);
	shader_.setFloat("material.shininess", 16.00f);
    shader_.setFloat("material.specular", 0.3f);
	shader_.setVec3("dir_light.direction", 0.0f, -1.0f, -1.0f);
	shader_.setVec3("dir_light.ambient", 0.3f, 0.3f, 0.3f);
	shader_.setVec3("dir_light.diffuse", 0.7f, 0.7f, 0.7f);
	shader_.setVec3("dir_light.specular", 0.5f, 0.5f, 0.5f);
}

BoxEntity::~BoxEntity()
{

}

dMatrix& BoxEntity::get_tm(void)
{
    return tm_;
}

void BoxEntity::set_tm(dMatrix const& tm)
{
    tm_ = tm;
}

NewtonBody* const BoxEntity::get_dyn(void)
{
    return dyn_.get();
}

void BoxEntity::set_user_data(void* data)
{
    NewtonBodySetUserData(dyn_.get(), data);
}

void BoxEntity::draw(glm::mat4 const& pers, glm::mat4 const& view)
{
    shader_.use();
    // Perspective matrix
    glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(pers));
    // View matrix
    glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(view));
    // Model matrix
    glUniformMatrix4fv(2, 1, GL_FALSE, &tm_[0][0]);
    // Draw mesh
    primitive_.draw(shader_);
}
