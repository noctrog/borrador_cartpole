#include <Environment.h>
#include <rndr_core.h>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

namespace rl
{
    const int SCR_WIDTH = 800;
    const int SCR_HEIGHT = 600;
    const char* APP_NAME = "Environment";

    Environment::Environment()
    {
        setup();
    }

    Environment::~Environment()
    {
        close();
    }

    void Environment::setup(void)
    {
        init_graphics();
    }

    void Environment::close(void)
    {
        stop_graphics();
    }

    void Environment::fb_size_callback(const int width, const int height)
    {
        // Update OpenGL viewport
        glViewport(0, 0, width, height);
        // Update screen dimensions
        w_.width = width;
        w_.height = height;
        // Update perspective matrix
        update_perspective_matrix();
    }

    void Environment::init_graphics(void)
    {
        glfw_setup();
        opengl_setup();
    }

    void Environment::stop_graphics(void)
    {
        opengl_close();
        glfw_close();
    }

    void Environment::fb_size_callback(GLFWwindow* window, const int width, const int height)
    {
        // Get Application pointer
        Environment* env = static_cast<Environment*>(glfwGetWindowUserPointer(window));
        env->fb_size_callback(width, height);
    }
    
    void Environment::cursor_pos_callback(GLFWwindow* window, double xpos, double ypos)
    {

    }

    void Environment::scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
    {

    }

    void Environment::update_perspective_matrix(void)
    {
        perspective_ = glm::perspective(70.0f,
                                        static_cast<float>(w_.width) / static_cast<float>(w_.height),
                                        0.01f, 100.0f);
    }

    void Environment::glfw_setup(void)
    {
        glfwInit();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        window_.reset(glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, APP_NAME, nullptr, nullptr));

        if (!window_) {
            std::cerr << "Failed to create GLFW window" << std::endl;
            glfwTerminate();
            exit(-1);
        }

        glfwMakeContextCurrent(window_.get());
        // Make application methods visible within callbacks
        glfwSetWindowUserPointer(window_.get(), this);
        // Set up callbacks
        glfwSetFramebufferSizeCallback(window_.get(), fb_size_callback);
        glfwSetCursorPosCallback(window_.get(), cursor_pos_callback);
        glfwSetScrollCallback(window_.get(), scroll_callback);

        glfwSetInputMode(window_.get(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        // Load all OpenGL functions
        if (!rndr::load_gl_functions(glfwGetProcAddress)) {
            std::cerr << "Failed to initialize GLAD" << std::endl;
            exit(-1);
        }
    }

    void Environment::opengl_setup(void)
    {
        // Enable depth test
        glEnable(GL_DEPTH_TEST);
    }

    void Environment::glfw_close(void)
    {
        glfwTerminate();
    }

    void Environment::opengl_close(void)
    {

    }
}
