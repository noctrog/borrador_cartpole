#include <iostream>
#include <random>

#include <CartPoleEnv.h>

#include <torch/torch.h>

const int MAX_STEPS = 600;
const int HIDDEN_SIZE = 128;
const int BATCH_SIZE  = 32;
const float PERCENTILE = 0.5;

struct EpisodeStep {
    rl::CartPoleEnv::Observation obs;
    rl::CartPoleEnv::Action a;
};

struct Episode {
    float reward;
    std::vector<EpisodeStep> steps;
};

const torch::Device device = torch::kCPU;

struct Net : torch::nn::Module {
    Net(int input_size, int hidden_size, int output_size) 
    {
        fc1 = register_module("fc1", torch::nn::Linear(input_size, hidden_size));
        fc2 = register_module("fc2", torch::nn::Linear(hidden_size, output_size));
    }

    torch::Tensor forward(torch::Tensor x)
    {
        x = torch::relu(fc1->forward(x));
        x = fc2->forward(x);

        return x;
    }

    torch::nn::Linear fc1{nullptr}, fc2{nullptr};
};

auto generate_batch(rl::CartPoleEnv& env, Net& net)
{
    // Random device for calculating actions
    std::random_device rd{};
    auto mtgen = std::mt19937 { rd() };

    std::vector<Episode> batch;
    batch.reserve(BATCH_SIZE);

    for (size_t i = 0; i < BATCH_SIZE; ++i) {
        Episode e;
        e.reward = 0.0f;
        e.steps.reserve(MAX_STEPS);
        // Prepare new environment for episode
        auto obs = env.reset();

        bool is_done = false;
        for (size_t step = 0; not is_done; ++step) {
            // Initialize step
            EpisodeStep s;
            // Get current observation
            s.obs = obs;
            float r;
            // Compute action for given observation and save
            auto actions = torch::softmax(net.forward(torch::from_blob(obs.data(), 
                        {1, obs.size()}).clone().to(device)), 1).cpu();
            auto ac = actions.accessor<float, 2>();
            //std::cout << "Probs: " << ac[0][0] << ", " << ac[0][1] << std::endl;
            auto dist = std::discrete_distribution<size_t> { ac[0][0], ac[0][1] };
            s.a = static_cast<rl::CartPoleEnv::Action>( dist(mtgen) );

            // Get next observation and reward
            std::tie(s.obs, r, is_done) = env.step(s.a);
            // Update reward
            e.reward += r;
            // Save current step
            e.steps.push_back(std::move(s));
        }

        batch.push_back(std::move(e));
    }

    return batch;
}

auto filter_batch(std::vector<Episode> batch, float percentile)
    -> std::tuple<torch::Tensor, torch::Tensor, float, float>
{
    // List of rewards
    std::vector<float> rewards(batch.size(), 0.0f);
    std::transform(batch.begin(), batch.end(), rewards.begin(),
                   [](Episode const& i){return i.reward;});
    // Percentile bound
    const size_t bound_index = std::floor(static_cast<float>(BATCH_SIZE) * PERCENTILE);
    std::nth_element(batch.begin(), batch.begin() + bound_index, batch.end(), 
            [](Episode const& e1, Episode const& e2){return e1.reward > e2.reward;});
    const auto bound_reward = batch[bound_index].reward;
    // Mean reward
    const auto mean_reward = std::accumulate(rewards.begin(), rewards.end(), 0.0f) / rewards.size();

    std::cout << *std::min_element(rewards.begin(), rewards.end()) << " " <<
                 mean_reward << " " << bound_reward << " " << 
                 *std::max_element(rewards.begin(), rewards.end()) << std::endl;
    // Generate observation and action pairs
    const size_t valid_episodes = BATCH_SIZE - bound_index;
    auto train_obs = std::vector<rl::CartPoleEnv::Observation>();
    auto train_act = std::vector<rl::CartPoleEnv::Action>();
    train_obs.reserve(valid_episodes * MAX_STEPS);
    train_act.reserve(valid_episodes * MAX_STEPS);
    for (auto it = batch.begin() + bound_index; it != batch.end(); ++it) {
        for (size_t i = 0; i < it->steps.size(); ++i) {
            train_obs.push_back(it->steps[i].obs);
            train_act.push_back(it->steps[i].a);
        }
    }

    long n_batches = train_obs.size();
    // Create tensors
    torch::Tensor train_obs_v = torch::from_blob(train_obs.data()->data(), {n_batches, 4}, at::kFloat)
        .clone().to(device);
    torch::Tensor train_act_v = torch::from_blob(train_act.data(), {n_batches}, at::kLong)
        .clone().to(device);

    return {train_obs_v, train_act_v, bound_reward, mean_reward};
}

int main(int argc, char *argv[])
{
    // Random device
    std::random_device rd {};
    auto mtgen = std::mt19937{ rd() };
    auto ud = std::uniform_int_distribution<uint32_t>(0, 2);

    // Neural network
    Net net { 4, 128, 2 };
    auto objective = torch::nn::CrossEntropyLoss();
    auto optimizer = torch::optim::Adam(net.parameters(), 0.01);

    // Environment
    auto env = rl::CartPoleEnv(MAX_STEPS);
    glfwSwapInterval(0);

    float current_score = 0.0f;
    while (current_score < MAX_STEPS - 1) {
        auto batch = generate_batch(env, net);
        auto [obs_v, act_v, bound_reward, mean_reward] 
            = filter_batch(batch, PERCENTILE);
        current_score = mean_reward;

        optimizer.zero_grad();
        auto action_scores_v = net.forward(obs_v);
        auto loss_v = objective(action_scores_v, act_v);
        loss_v.backward();
        optimizer.step();

        //std::cout << mean_reward << std::endl;
    }

    return 0;
}
