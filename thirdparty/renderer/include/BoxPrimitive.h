#pragma once

#include <Primitive.h>
#include <glm/glm.hpp>
#include <string_view>

namespace rndr
{
class BoxPrimitive : Primitive
{
    public:
        // args: Size, textures
        BoxPrimitive(glm::vec3 const& _size,
                     std::string_view tex_diffuse = "",
                     std::string_view tex_normal = "");
        virtual ~BoxPrimitive();

        virtual void draw(Shader const& shader) const override;
    private:
        void create_mesh(glm::vec3 const& _size,
                         std::string_view tex_diffuse = "",
                         std::string_view tex_normal  = "");
        std::vector<Vertex> create_vertices(glm::vec3 const& _size) const;
        std::vector<uint32_t> create_indices() const;
        std::vector<Texture> create_textures() const;

        glm::vec3 size_;

        GLuint tex_diffuse_;
        GLuint tex_normal_;
};

}

