#pragma once

#include <string>
#include <vector>
#include <memory>
#include <inttypes.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <Shader.h>
#include <Texture.h>

namespace rndr
{
    struct Vertex 
    {
        glm::vec3 Position;
        glm::vec3 Normal;
        glm::vec2 TexCoords;
    };

    class Mesh
    {
    public:
        Mesh (std::vector<Vertex> vertices, std::vector<uint32_t> indices, std::vector<Texture> textures);
        virtual ~Mesh ();

        void draw(Shader const& shader);

    private:
        uint32_t VAO, VBO, EBO;

        std::vector<Vertex> vertices;
        std::vector<uint32_t> indices;
        std::vector<Texture> textures;

        void setup_mesh();
    };
}
