#pragma once

#include <memory>
#include <Mesh.h>

namespace rndr
{
class Shader;
class Mesh;

class Primitive
{
    public:
        Primitive();
        virtual ~Primitive() = 0;

        virtual void draw(Shader const& shader) const = 0;
    protected:
        std::unique_ptr<Mesh> mesh_;
};

}

