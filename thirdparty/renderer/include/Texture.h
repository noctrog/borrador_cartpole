#pragma once

#include <string>
#include <glad/glad.h>

namespace rndr
{
    namespace
    {

    }

    class Texture
    {
        public:
            enum class Type : unsigned int 
            {
                DIFFUSE = 0,
                NORMAL,
                SPECULAR,
                HEIGHT,
                N_TYPES_TEXTURES
            };

            Texture(std::string_view path, Type type);
            virtual ~Texture();

            void load_from_file(std::string_view path);
            std::string get_tex_type(void) const;
            GLuint get(void) const;

            operator GLuint() const;
        private:
            GLuint tex_id_;
            Type tex_type_;
    };
}
