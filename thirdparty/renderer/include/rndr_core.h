#pragma once

namespace rndr
{
    // GLFW
    bool load_gl_functions(void (*func(const char*))());
}
