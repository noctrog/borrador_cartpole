#include <numeric>
#include <BoxPrimitive.h>
#include <Mesh.h>
#include <stb_image.h>
#include <iostream>

namespace rndr
{
    namespace 
    {
        const float v[] = {
            // positions          // normals           // texture coords
            -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
            0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
            -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,

            -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
            -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

            -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
            -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
            -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
            -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
            -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
            -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

            0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
            0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
            0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
            0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
            0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

            -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,
            0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f,
            0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,

            -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f
        };
    }

    BoxPrimitive::BoxPrimitive(glm::vec3 const& _size,
                               std::string_view tex_diffuse,
                               std::string_view tex_normal)
        : size_(_size)
    {
        create_mesh(_size, tex_diffuse, tex_normal);
    }

    BoxPrimitive::~BoxPrimitive()
    {

    }

    void BoxPrimitive::draw(Shader const& shader) const
    {
        mesh_->draw(shader);
    }

    void BoxPrimitive::create_mesh(glm::vec3 const& size,
                                   std::string_view tex_diffuse,
                                   std::string_view tex_normal)
    {
        auto vertices = create_vertices(size_);
        auto indices = create_indices();
        auto textures = std::vector<Texture>();

        if (!tex_diffuse.empty()) {
            textures.emplace_back(tex_diffuse, Texture::Type::DIFFUSE);
        }

        // Create mesh
        mesh_.reset(new Mesh(vertices, indices, textures));
    }

    std::vector<Vertex> BoxPrimitive::create_vertices(glm::vec3 const& _size) const
    {
        // Number of floats per vertex (position, normal, texcoords)
        constexpr size_t stride = sizeof(Vertex) / sizeof(float);
        // Number of vertices
        constexpr size_t n_verts = sizeof(v) / sizeof(Vertex);

        // Save vertices for their use with the renderer
        std::vector<Vertex> vertices;
        vertices.reserve(n_verts);
        for (size_t i = 0; i < n_verts; ++i) {
            glm::vec3 c_vertices { size_.x * v[i*stride + 0], 
                                   size_.y * v[i*stride + 1], 
                                   size_.z * v[i*stride + 2] };
            glm::vec3 c_normals  { v[i*stride + 3], v[i*stride + 4], v[i*stride + 5] };
            glm::vec2 c_texcoord { v[i*stride + 6], v[i*stride + 5] };

            vertices.emplace_back(Vertex { .Position = c_vertices, 
                                           .Normal = c_normals,
                                           .TexCoords = c_texcoord });
        }

        return vertices;
    }

    std::vector<uint32_t> BoxPrimitive::create_indices() const
    {
        // Number of vertices
        constexpr size_t n_verts = sizeof(v) / sizeof(Vertex);

        // Elements buffer
        std::vector<uint32_t> indices(n_verts);
        std::iota(indices.begin(), indices.end(), 0);

        return indices;
    }

    std::vector<Texture> BoxPrimitive::create_textures() const
    {
        return {};
    }
}
