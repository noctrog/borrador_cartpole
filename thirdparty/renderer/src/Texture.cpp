#include <Texture.h>
#include <stb_image.h>
#include <iostream>

namespace rndr
{
    static const char* tex_types[] =  
    {"texture_diffuse",
        "texture_normal",
        "texture_specular",
        "texture_height",
        ""};

    Texture::Texture(std::string_view path, Type type)
        : tex_type_(type)
    {
        load_from_file(path);
    }

    Texture::~Texture()
    {
        glDeleteTextures(1, &tex_id_);
    }

    void Texture::load_from_file(std::string_view path)
    {
        // Create texture
        glGenTextures(1, &tex_id_);
        glBindTexture(GL_TEXTURE_2D, tex_id_);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // Load texture from file
        int width, height, nrComponents;
        uint8_t *data = stbi_load(path.data(), &width, &height, &nrComponents, 0);
        if (data) {
            GLenum format;
            switch (nrComponents) {
                case 1:
                    format = GL_RED;
                    break;
                case 3:
                    format = GL_RGB;
                    break;
                case 4:
                    format = GL_RGBA;
                    break;
                default:
                    break;
            }	

            glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);

            // Free CPU memory
            stbi_image_free(data);
        }
        else {
            std::cerr << "Texture failed to load at path: " << path << std::endl;
            // Free CPU memory
            stbi_image_free(data);
            // Delete texture
            glDeleteTextures(1, &tex_id_);
        }
    }

    std::string Texture::get_tex_type(void) const
    {
        return std::string(tex_types[static_cast<unsigned int>(tex_type_)]);
    }

    GLuint Texture::get() const
    {
        return tex_id_;
    }

    Texture::operator GLuint() const
    {
        return tex_id_;
    }
}
