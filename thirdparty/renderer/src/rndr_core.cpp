#include <glad/glad.h>

namespace rndr
{
    bool load_gl_functions(void (*func(const char*))())
    {
        bool success = true;

        success = gladLoadGLLoader((GLADloadproc)func);

        return success;
    }
}
