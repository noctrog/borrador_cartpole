#version 450 core

layout (location = 0) in vec3 aPos;

layout (location = 0) uniform mat4 p;
layout (location = 1) uniform mat4 v;
layout (location = 2) uniform mat4 m;

void main(void)
{
	gl_Position = v * p * m * vec4(aPos, 1.0f);
}
