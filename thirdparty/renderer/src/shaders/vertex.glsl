#version 450 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoord;

out vec2 TexCoord;

out vec3 FragPos;
out vec3 Normal;

layout (location = 0) uniform mat4 p;
layout (location = 1) uniform mat4 v;
layout (location = 2) uniform mat4 m;

void main()
{
    gl_Position = v * p * m * vec4(aPos, 1.0);
    TexCoord = aTexCoord;
    Normal = mat3(transpose(inverse(m))) * aNormal;
    FragPos = vec3(m * vec4(aPos, 1.0f));
}
